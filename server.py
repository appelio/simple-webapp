#!/usr/bin/env python

from flask import (
    Flask,
    render_template
)

import socket

# Create the application instance
app = Flask(__name__, template_folder=".")

# Create a URL route in our application for "/"
@app.route('/')
def home():
    return render_template('home.html', hostname=socket.gethostname())

# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
